'use strict';

const Boom = require('@hapi/boom');
const Jwt = require('@hapi/jwt');
const Joi = require('joi');

module.exports = [
    {
        method: 'post',
        path: '/movie',
        options: {
            tags: ['api'],
            validate: {
                payload: Joi.object({
                    title: Joi.string().min(3).example('Pulp Fiction').description('Title of the movie'),
                    genre: Joi.string().min(3).example('Thriller').description('Main genre of the movie'),
                    synopsis: Joi.string().min(3).example('L\'odyssée sanglante et burlesque de petits malfrats dans la jungle de Hollywood à travers trois histoires qui s\'entremêlent.').description('Synopsis of the movie'),
                    director: Joi.string().min(3).example('Quentin Tarantino').description('Director of the movie'),
                    releaseYear: Joi.number().example(1994).description('Release year of the movie'),
                    runningTime: Joi.number().example(154).description('Running time of the movie in minutes')
                })
            },
            auth: {
                scope: ['admin']
            }
        },
        handler: async (request, h) => {

            const { movieService } = request.services();

            const movieAdded = await movieService.create(request.payload);

            const { userService } = request.services();
            const { mailService } = request.services();

            const listAllUsers = await userService.list();
            listAllUsers.forEach(async (user) => {
                await mailService.sendEmailOnMovieCreation(user.mail);
            });

            return movieAdded;
        }
    },
    {
        method: 'get',
        path: '/movie',
        options: {
            tags: ['api'],
            auth: {
                scope: ['admin', 'user']
            }
        },
        handler: async (request, h) => {
            const { movieService } = request.services();

            return await movieService.list();
        }
    },
    {
        method: 'patch',
        path: '/movie/edit/{idMovie}',
        options: {
            tags: ['api'],
            validate: {
                params: Joi.object({
                    idMovie: Joi.number().required().description('ID of the movie')
                }),
                payload: Joi.object({
                    title: Joi.string().min(3).example('').description('Title of the movie'),
                    genre: Joi.string().min(3).example('').description('Main genre of the movie'),
                    synopsis: Joi.string().min(3).example('').description('Synopsis of the movie'),
                    director: Joi.string().min(3).example('').description('Director of the movie'),
                    releaseYear: Joi.number().example('').description('Release year of the movie'),
                    runningTime: Joi.number().example('').description('Running time of the movie in minutes')
                })
            },
            auth: {
                scope: ['admin']
            }
        },
        handler: async (request, h) => {
            const { movieService } = request.services();

            const movieEdited = await movieService.edit(request.params.idMovie, request.payload);

            const { favoriteService } = request.services();
            const { mailService } = request.services();

            const listUsersWithMovieInFavorite = await favoriteService.getUserListWithMovieInFavorite(request.params.idMovie);
            listUsersWithMovieInFavorite.forEach(async (user) => {
                await mailService.sendEmailOnMovieEdition(user.mail);
            });

            return movieEdited;
        }
    }
];
