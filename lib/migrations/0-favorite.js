'use strict';

module.exports = {

    async up(knex) {

        await knex.schema.createTable('favorite', (table) => {

            table.increments('id').primary();
            table.integer('idUser').notNull();
            table.integer('idMovie').notNull();

            table.foreign('idUser').references('id').inTable('user');
            table.foreign('idMovie').references('id').inTable('movie');
        });
    },

    async down(knex) {

        await knex.schema.dropTableIfExists('favorite');
    }
};