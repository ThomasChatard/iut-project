'use strict';

const { Service } = require('schmervice');

module.exports = class MovieService extends Service {

    create(movie) {

        const { Movie } = this.server.models();

        return Movie.query().insertAndFetch(movie);
    }

    list() {
        const { Movie } = this.server.models();

        return Movie.query();
    }

    edit(idMovie, data) {
        const { Movie } = this.server.models();

        return Movie.query().where('id',idMovie).update(data);
    }
};