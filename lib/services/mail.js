'use strict';

const { Service } = require('schmervice');
const nodemailer = require('nodemailer');

module.exports = class MailService extends Service {

    transporter = nodemailer.createTransport({
        host: 'smtp.ethereal.email',
        port: 587,
        auth: {
            user: 'charles52@ethereal.email',
            pass: 'ZtX9KqdMyypY8fq81b'
        }
    });

    async sendEmailOnAccountCreation(mail) {
        return mail = await this.transporter.sendMail({
            from: 'iutnodeproject.admin@unilim.fr',
            to: mail,
            subject: 'Account creation',
            html: '<p>Bonjour ! Nous avons le plaisir de vous informer que votre demande de création de compte a bien été traitée.</p>'
        });
    }

    async sendEmailOnMovieCreation(mail) {
        return mail = await this.transporter.sendMail({
            from: 'iutnodeproject.admin@unilim.fr',
            to: mail,
            subject: 'New movie',
            html: '<p>Bonjour ! Nous avons le plaisir de vous informer qu\'un nouveau film a été ajouté a notre base de données !</p>'
        });
    }

    async sendEmailOnMovieEdition(mail) {
        return mail = await this.transporter.sendMail({
            from: 'iutnodeproject.admin@unilim.fr',
            to: mail,
            subject: 'Movie update',
            html: '<p>Bonjour ! Nous avons le plaisir de vous informer qu\'un film présent dans votre liste de favoris a été mis à jour !</p>'
        });
    }

};
