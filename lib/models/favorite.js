'use strict';

const Joi = require('joi');
const { Model } = require('schwifty');

module.exports = class Favorite extends Model {

    static get tableName() {
        return 'favorite';
    }

    static get joiSchema() {

        return Joi.object({
            id: Joi.number().integer().greater(0),
            idUser: Joi.number().example(1).description('Id of the user'),
            idMovie: Joi.number().example(1).description('Id of the movie'),
        });
    }

};