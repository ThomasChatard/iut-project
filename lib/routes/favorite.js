'use strict';

const Boom = require('@hapi/boom');
const Jwt = require('@hapi/jwt');
const Joi = require('joi');

module.exports = [
    {
        method: 'post',
        path: '/favorite',
        options: {
            tags: ['api'],
            validate: {
                payload: Joi.object({
                    idMovie: Joi.number().integer().example(1).description('Id of the movie'),
                })
            },
            auth: {
                scope: ['user']
            }
        },
        handler: async (request, h) => {

            const { favoriteService } = request.services();

            const idUser = request.auth.credentials.id;
            const idMovie = request.payload.idMovie;
            const isAlreadyInFavoriteList = await favoriteService.isAlreadyInFavoriteList(idUser,idMovie);

            if (isAlreadyInFavoriteList){
                return Boom.notAcceptable('This movie is already in your favorite list');
            }

            return await favoriteService.addToFavorite(idUser,idMovie);
        }
    },
    {
        method: 'delete',
        path: '/favorite/{idMovie}',
        options: {
            tags: ['api'],
            validate: {
                payload: Joi.object({
                    idMovie: Joi.number().required().description('ID of the movie')
                })
            },
            auth: {
                scope: ['user']
            }
        },
        handler: async (request, h) => {
            const { favoriteService } = request.services();

            const idUser = request.auth.credentials.id;
            const idMovie = request.payload.idMovie;
            const isAlreadyInFavoriteList = await favoriteService.isAlreadyInFavoriteList(idUser,idMovie);

            if (!isAlreadyInFavoriteList){
                return Boom.notAcceptable('This movie is not in your favorite list');
            }

            return await favoriteService.removeFromFavorite(idUser,idMovie);
        }
    }
];
