'use strict';

const { Service } = require('schmervice');

module.exports = class FavoriteService extends Service {

    addToFavorite(idUser, idMovie) {
        const { Favorite } = this.server.models();

        const newFavorite = new Favorite();
        newFavorite.idUser = idUser;
        newFavorite.idMovie = idMovie;

        return Favorite.query().insertAndFetch(newFavorite);
    }

    removeFromFavorite(idUser, idMovie) {
        const { Favorite } = this.server.models();

        return Favorite.query().where('idUser', idUser).where('idMovie',idMovie).del();
    }

    async isAlreadyInFavoriteList(idUser, idMovie) {
        const { Favorite } = this.server.models();

        const result = await Favorite.query().where('idUser', idUser).where('idMovie',idMovie);
        console.log(result);
        if (result[0] == undefined) {
            return false;
        } 

        return true;
        
    }

    getUserListWithMovieInFavorite(idMovie) {
        const { Favorite } = this.server.models();

        return Favorite.query().where('idMovie',idMovie);
    }
};
