'use strict';

const { Service } = require('schmervice');

module.exports = class UserService extends Service {

    create(user) {

        const { User } = this.server.models();

        return User.query().insertAndFetch(user);
    }

    list() {
        const { User } = this.server.models();

        return User.query();
    }

    delete(idUser) {
        const { User } = this.server.models();

        return User.query().where('id',idUser).del();
    }

    edit(idUser, data) {
        const { User } = this.server.models();

        return User.query().where('id',idUser).update(data);
    }

    login(mailUser) {
        const { User } = this.server.models();

        return User.query().where('mail',mailUser);
    }


}