'use strict';

const Joi = require('joi');
const { Model } = require('schwifty');

module.exports = class Movie extends Model {

    static get tableName() {
        return 'movie';
    }

    static get joiSchema() {

        return Joi.object({
            id: Joi.number().integer().greater(0),
            title: Joi.string().min(3).example('Pulp Fiction').description('Title of the movie'),
            genre: Joi.string().min(3).example('Thriller').description('Main genre of the movie'),
            synopsis: Joi.string().min(3).example('L\'odyssée sanglante et burlesque de petits malfrats dans la jungle de Hollywood à travers trois histoires qui s\'entremêlent.').description('Synopsis of the movie'),
            director: Joi.string().min(3).example('Quentin Tarantino').description('Director of the movie'),
            releaseYear: Joi.number().example(1994).description('Release year of the movie'),
            runningTime: Joi.number().example(154).description('Running time of the movie in minutes'),
            createdAt: Joi.date(),
            updatedAt: Joi.date()
        });
    }

    $beforeInsert(queryContext) {

        this.updatedAt = new Date();
        this.createdAt = this.updatedAt;
    }

    $beforeUpdate(opt, queryContext) {

        this.updatedAt = new Date();
    }

};