'use strict';

const Boom = require('@hapi/boom');
const Jwt = require('@hapi/jwt');
const Joi = require('joi');

module.exports = [
    {
        method: 'post',
        path: '/user',
        options: {
            tags: ['api'],
            validate: {
                payload: Joi.object({
                    firstName: Joi.string().required().min(3).example('John').description('Firstname of the user'),
                    lastName: Joi.string().required().min(3).example('Doe').description('Lastname of the user'),
                    username: Joi.string().min(3).example('John_Doe').description('Username of the user'),
                    mail: Joi.string().min(3).email().example('john.doe@mail.com').description('Email of the user'),
                    password: Joi.string().min(8).example('John_Doe').description('Password of the user')
                })
            },
            auth: false
        },
        handler: async (request, h) => {

            const { userService } = request.services();

            const userCreated = await userService.create(request.payload);

            const { mailService } = request.services();

            await mailService.sendEmailOnAccountCreation(userCreated.mail);

            return userCreated;
        }
    },
    {
        method: 'get',
        path: '/user',
        options: {
            tags: ['api'],
            auth: {
                scope: ['admin', 'user']
            }
        },
        handler: async (request, h) => {
            const { userService } = request.services();

            return await userService.list();
        }
    },
    {
        method: 'delete',
        path: '/user/{idUser}',
        options: {
            tags: ['api'],
            validate: {
                params: Joi.object({
                    idUser: Joi.number().required().description('ID of the user')
                })
            },
            auth: {
                scope: ['admin']
            }
        },
        handler: async (request, h) => {
            const { userService } = request.services();

            const del = await userService.delete(request.params.idUser);

            if (del) {
                return '';
            }

            return 'Error';
        }
    },
    {
        method: 'patch',
        path: '/user/edit/{idUser}',
        options: {
            tags: ['api'],
            validate: {
                params: Joi.object({
                    idUser: Joi.number().required().description('ID of the user')
                }),
                payload: Joi.object({
                    firstName: Joi.string().example('').min(3).description('New Firstname'),
                    lastName: Joi.string().example('').min(3).description('New Lastname'),
                    username: Joi.string().example('').min(3).description('New Username'),
                    mail: Joi.string().example('').min(3).email().description('New Email')
                })
            },
            auth: {
                scope: ['admin']
            }
        },
        handler: async (request, h) => {
            const { userService } = request.services();

            return await userService.edit(request.params.idUser, request.payload);
        }
    },
    {
        method: 'post',
        path: '/user/login',
        options: {
            tags: ['api'],
            validate: {
                payload: Joi.object({
                    mail: Joi.string().min(3).email().example('').description('Email of the user'),
                    password: Joi.string().min(8).example('').description('Password of the user')
                })
            },
            auth: false
        },
        handler: async (request, h) => {
            const { userService } = request.services();
            const result = await userService.login(request.payload.mail);

            const passwordUser = result[0].password;
            if (passwordUser === request.payload.password) {
                return Jwt.token.generate(
                    {
                        aud: 'urn:audience:iut',
                        iss: 'urn:issuer:iut',
                        id: result[0].id,
                        firstName: result[0].firstName,
                        lastName: result[0].lastName,
                        mail: result[0].mail,
                        scope: result[0].scope
                    },
                    {
                        key: 'random_string', // La clé qui est définit dans lib/auth/strategies/jwt.js
                        algorithm: 'HS512'
                    },
                    {
                        ttlSec: 14400 // 4 hours
                    }
                );
            }

            return Boom.unauthorized('Wrong mail / password');
        }
    }
];
